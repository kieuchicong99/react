import React from "react";
import InputSelect from "localstorage_input_select/dist/InputSelect";
import "./index.css";
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputVal: "",
    };
  }

  handleInputValue = (value) => {
    this.setState({ inputVal: value });
  };

  render() {
    return (
      <div>
        <InputSelect
          storeKey="search"
          placeholderName="Search"
          handleInput={this.handleInputValue}
        ></InputSelect>
        <div>chooseValue: {this.state.inputVal}</div>
      </div>
    );
  }
}
